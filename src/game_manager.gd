extends Node

var Entity = preload("res://scenes/entity.tscn")
var Tilemap = preload("res://src/tilemap.gd")
var SpriteSheet = preload("res://src/spritesheet.gd")
var Command = preload("res://src/command.gd")
var Client = preload("res://src/client.gd")
var Message = preload("res://src/message.gd")

var Level = preload("res://scenes/level.tscn")
var Login = preload("res://scenes/login.tscn")
var Register = preload("res://scenes/register.tscn")

var entities = {}
var settings = {}
var spritesheet

var portrait = null
var portrait_hp = null
var portrait_name = null
var portrait_sprite = null

var target = null
var target_portrait = null
var target_portrait_hp = null
var target_portrait_name = null
var target_portrait_sprite = null

var current_map = null
var connection_status = null
var username = "NO NAME"
var graphic = "player0"
var player = null
var chat = null
var chat_messages = []
var login_root = null

func _ready():
	var filename = "res://data/game.json"
	var file = File.new()
	file.open(filename, File.READ)
	if file.is_open():
		settings.parse_json(file.get_as_text())
	else:
		print("could not open ", filename)
		return
	file.close()

	spritesheet = SpriteSheet.new()
	spritesheet.init(settings)

	Peer.connect_request(settings["ServerAddr"], settings["ServerPort"])
	
	current_map = Tilemap.new()
	current_map.load_from_tiled("res://data/tilemaps/level0.json")
	self.add_child(current_map)

	set_process(true)
	set_process_input(true)
	
func _input(event):
	if !Peer.connected:
		return
	if event.is_action_pressed("move_right"):
		send_command(Command.Move.new(Peer.id, Vector2(1, 0)))
	elif event.is_action_pressed("move_left"):
		send_command(Command.Move.new(Peer.id, Vector2(-1, 0)))
	elif event.is_action_pressed("move_up"):
		send_command(Command.Move.new(Peer.id, Vector2(0, -1)))
	elif event.is_action_pressed("move_down"):
		send_command(Command.Move.new(Peer.id, Vector2(0, 1)))
	if event.type == InputEvent.MOUSE_BUTTON:
		if event.button_index == BUTTON_LEFT && event.pressed:
			handle_left_click()

func _process(delta):
	for e in self.entities:
		self.entities[e].update(delta)
	
func new_id():
	return OS.get_ticks_msec()
	
func connect_request(username, password):
	self.username = username
	if login_root != null:
		login_root.init("Connecting...", true)
	var packet = Peer.new_packet(Peer.Message.Connect, {"Username": username, "Password": password.sha256_text() })
	Peer.send_packet(packet)

func load_tilemap(level):
	self.remove_child(current_map)	
	current_map = Tilemap.new()
	current_map.load_from_tiled(settings["Tilemaps"][level])
	self.add_child(current_map)

func new_entity_with_id(id, graphic):
	var entity = Entity.instance()
	entity.init(id, graphic, Vector2(0, 0), spritesheet)
	self.entities[id] = entity
	self.add_child(entity)
	return id

func at_pos(id, position):
	var entity = self.entities[id]
	entity.set_pos(position)
	
func get_entity_ref_by_id(id):
	return self.entities[id]

func send_command(command, send=true):
	var id = command.id
	if !self.entities.has(id):
		return false
	var entity = self.entities[id]
	var result = true
	if command.kind == Command.Kind.Move:
		var tiles = current_map.get_tiles(entity.pos + command.offset)
		for tile in tiles:
			if tile.solid:
				result = false
				break
	elif command.kind == Command.Kind.SetPos:
		var tiles = current_map.get_tiles(command.pos)
		for tile in tiles:
			if tile.solid:
				result = false
				break

	if entity == player:
		var roof = GameManager.current_map.get_layer("roof")
		if roof != null:
			if is_underneath_layer(roof):
				GameManager.current_map.set_layer_visible("roof", false)
			else:
				GameManager.current_map.set_layer_visible("roof", true)

	if result:
		if send && id == Peer.id:
			Peer.send_packet(Peer.new_packet(Client.Message.PlayerMoved, {"Data": {"ID": Peer.id, "Offset": [command.offset.x, command.offset.y]}}))
		return entity.send_command(command)

func remove_entity_by_id(id):
	remove_child(self.entities[id])	
	self.entities.erase(id)

func is_underneath_layer(layer):
	for tile in layer.get_children():
		# get a Rectangle that uses the tiles position
		var region = Rect2(tile.get_global_pos(), tile.get_region_rect().size)
		if region.pos.x == player.get_pos().x && region.pos.y == player.get_pos().y:
			return true
	return false

func handle_left_click():
	var global_pos = player.get_global_mouse_pos()
	print(global_pos)
	# use a QuadTree to optimize
	for entity in self.entities.values():
		if entity != player:
			var region = Rect2(entity.get_global_pos(), entity.get_global_region_rect().size)
			print(region)
			if region.has_point(global_pos):
				set_target(entity)
				return
	set_target(null)

func set_target(entity):
	if entity == null:
		target_portrait.set_hidden(true)
		if target != null:
			target.find_node("target").set_hidden(true)
			target.find_node("name_layer").set_hidden(true)
		target = entity	
		return
	target = entity	
	target.find_node("target").set_hidden(false)
	target.find_node("name_layer").set_hidden(false)
	target_portrait.set_hidden(false)
	target_portrait_name.set_text(entity.name)
	target_portrait_sprite.set_texture(entity.get_texture())
	target_portrait_sprite.set_region(true)
	target_portrait_sprite.set_region_rect(entity.get_region_rect())	

func initialize_player(id, graphic, position):
	# don't load level until we're actually connected
	get_tree().change_scene_to(Level)

	load_tilemap("level1")
	self.new_entity_with_id(id, graphic)
	self.entities[id].set_is_player(true)
	self.entities[id].set_name(username)
	self.entities[id].set_graphic(graphic)
	self.entities[id].set_pos(position)
	self.entities[id].camera.make_current()
	player = self.get_entity_ref_by_id(id)

func level_ready(root):
	portrait = root.find_node("portrait")
	if portrait == null:
		print("Could not find portrait")
		return
	portrait.set_hidden(false)
	portrait_hp = portrait.find_node("health")
	portrait_name = portrait.find_node("name")
	portrait_name.set_text(username)
	portrait_sprite = portrait.find_node("sprite")
	portrait_sprite.set_texture(player.get_texture())
	portrait_sprite.set_region(true)
	portrait_sprite.set_region_rect(player.get_region_rect())

	target_portrait = root.find_node("target_portrait")
	target_portrait_hp = target_portrait.find_node("health")
	target_portrait_name = target_portrait.find_node("name")
	target_portrait_sprite = target_portrait.find_node("sprite")

func set_chat_panel(panel):
	self.chat = panel
	if chat_messages.size() > 0:
		for msg in chat_messages:
			add_chat_message(msg[0], msg[1])

func send_chat_message(message):
	Peer.send_packet(Peer.new_packet(Peer.Message.ChatMessage, {"Username": username, "Message": message}))

func add_chat_message(username, message):
	if chat != null:
		var label = Label.new()
		label.set_script(Message)
		label.init(username, message)
		chat.add_child(label)
	else:
		chat_messages.append([username, message])

var register_success = false
func set_register_successful():
	get_tree().change_scene_to(Login)
	register_success = true

func login_ready(root):
	self.login_root = root
	if register_success:
		root.init("Registered successfully")
	elif timed_out:
		root.init("Lost connection to the server", false)

var timed_out = false
func peer_timed_out():
	timed_out = true
	if get_tree().get_current_scene() == self.login_root:
		login_ready(self.login_root)
	else:
		get_tree().change_scene_to(Login)