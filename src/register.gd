extends Control

var username
var password

var graphic = ""
var graphics
var next
var prev
var error_status

const max_graphics = 1
var graphics_count = 0

const max_username_length = 16
const min_username_length = 4

func _ready():
	username = find_node("username")
	password = find_node("password")
	graphics = find_node("graphics", true)
	var atlas = AtlasTexture.new()
	atlas.set_atlas(GameManager.spritesheet.texture)
	graphics.set_texture(atlas)
	graphics.set_scale(GameManager.spritesheet.scale)
	update_graphics_selection()
	error_status = find_node("error_status")
	next = find_node("next")
	prev = find_node("prev")

func register_reply(status):
	# called where status is a status code
	if status == Peer.RegisterStatus.UsernameTaken:
		error_status.set_text("Username already taken")
		error_status.set_hidden(false)
	elif status == Peer.RegisterStatus.UsernameTooShort:
		error_status.set_text("Username is too short")
		error_status.set_hidden(false)
	elif status == Peer.RegisterStatus.Successful:
		GameManager.set_register_successful()
	

func _on_register_button_pressed():
	if !username.get_text().empty() && !password.get_text().empty():
		if username.get_text().length() < min_username_length:
			error_status.set_text("Username is too short")
			error_status.set_hidden(false)
		else:
			Peer.connect("register_callback", self, "register_reply")
			Peer.register_request(username.get_text(), password.get_text(), graphic)

func _on_next_pressed():
	graphics_count += 1
	if graphics_count >= max_graphics:
		graphics_count = 0
	update_graphics_selection()

func _on_prev_pressed():
	graphics_count -= 1
	if graphics_count < 0:
		graphics_count = max_graphics - 1
	update_graphics_selection()

func update_graphics_selection():
	graphic = "player" + String(graphics_count)
	var rect = GameManager.spritesheet.get_rect(graphic)
	var tex = graphics.get_texture()
	tex.set_region(rect)
	graphics.set_texture(tex)