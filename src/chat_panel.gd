extends RichTextLabel

var scrollbar = null

func _ready():
	set_scroll_follow(true)
	GameManager.set_chat_panel(self)
	#scrollbar = get_parent().find_node("chat_scrollbar")

func add_child(child):
	add_text(child.get_text() + "\n")
	#.add_child(child)
	#scrollbar.set_max(self.get_child_count())