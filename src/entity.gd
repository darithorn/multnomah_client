extends Sprite

var Command = preload("res://src/command.gd")

var camera
var graphic = ""
var name = ""
var id = -1
var pos = Vector2(0, 0) # tilemap position
var spritesheet
var name_ui = null
var entity = null
var is_player = false
var anim = null

func init(id, graphic, pos, spritesheet):
	self.id = id
	self.graphic = graphic
	self.spritesheet = spritesheet
	self.set_pos(pos)
	camera = find_node("camera", true)	
	name_ui = find_node("name", true)
	anim = find_node("anim", true)
	set_texture(spritesheet.texture)
	set_region(true)
	set_region_rect(spritesheet.get_rect(graphic))
	set_scale(spritesheet.scale)

func set_pos(pos):
	self.pos = pos
	.set_pos(Vector2(pos.x * spritesheet.sprite_size.x * spritesheet.scale.x,
					pos.y * spritesheet.sprite_size.y * spritesheet.scale.y))

func get_pos():
	return .get_global_pos()

func set_global_pos(pos):
	self.pos = pos
	.set_global_pos(Vector2(pos.x * spritesheet.sprite_size.x * spritesheet.scale.x,
							pos.y * spritesheet.sprite_size.y * spritesheet.scale.y))

func move(offset):
	pos.x += offset.x
	pos.y += offset.y
	global_translate(Vector2(offset.x * spritesheet.sprite_size.x * spritesheet.scale.x,
							offset.y * spritesheet.sprite_size.y * spritesheet.scale.y))
	
func set_name(name):
	name_ui.set_text(name)
	self.name = name

func set_is_player(player):
	self.is_player = player

func set_graphic(graphic):
	self.graphic = graphic
	set_region_rect(spritesheet.get_rect(graphic))

func get_global_region_rect():
	var rect = .get_region_rect()
	return Rect2(get_pos(), rect.size * get_scale())

func update(delta):
	pass
	
func send_command(command):
	var result = false
	if command.kind == Command.Kind.Move:
		self.move(command.offset)
		if !anim.is_playing():
			anim.play("walk")
		result = true
	if command.kind == Command.Kind.SetPos:
		self.set_global_pos(command.pos)
		result = true

	return result