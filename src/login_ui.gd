extends Control

var spritesheet
var peer
var details
var username
var password
var good_status
var error_status 

func _ready():
	error_status = find_node("error_status")
	good_status = find_node("good_status")
	peer = find_node("client")
	details = find_node("details")
	username = details.find_node("username")
	password = details.find_node("password")
	GameManager.login_ready(self)
	Peer.connect("login_callback", self, "login_declined")

func init(text, good=true):
	good_status.set_hidden(true)
	error_status.set_hidden(true)
	if good:
		good_status.set_text(text)
		good_status.set_hidden(false)
	else:
		error_status.set_text(text)
		error_status.set_hidden(false)

func login_declined(status):
	if status == Peer.Declined.NotRegistered:
		init("User is not registered", false)
	elif status == Peer.Declined.WrongPassword:
		init("Wrong password", false)

func _on_login_pressed():
	if !username.get_text().empty() && !password.get_text().empty():
		GameManager.connect_request(username.get_text(), password.get_text())

func _on_register_pressed():
	get_tree().change_scene_to(GameManager.Register)