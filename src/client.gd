extends Node

var Command = preload("res://src/command.gd")

class Message:
	const Invalid = 0
	const Connect = 1
	const ConnectionChallenge = 2
	const ConnectionChallengeReply = 3
	const ConnectionDeclined = 4
	const ConnectionAccepted = 5
	const Acknowledge = 6
	const Correction = 7
	const PlayerJoined = 8
	const PlayerLeft = 9
	const PlayerMoved = 10
	const Register = 11
	const RegisterReply = 12
	const AddEntity = 13
	const CommandEntity = 14
	const ChatMessage = 15
	const Ping = 16

var MessageType = [
	"Invalid",
	"Connect",
	"ConnectionChallenge",
	"ConnectionChallengeReply",
	"ConnectionDeclined",
	"ConnectionAccepted",
	"Acknowledge",
	"Correction",
	"PlayerJoined",
	"PlayerLeft",
	"PlayerMoved", 
	"Register", 
	"RegisterReply",
	"AddEntity",
	"CommandEntity",
	"ChatMessage",
	"Ping", 
]

class RegisterStatus:
	const Successful = 0
	const UsernameTaken = 1
	const UsernameTooShort = 2	
	const UsernameTooLong = 3

class Declined:
	const WrongSalt = 0
	const AlreadyLoggedIn = 1
	const NotRegistered = 2
	const WrongPassword = 3

func format_msg_type(i):
	if i >= MessageType.size():
		return "Unknown"
	return MessageType[i]
	

class Packet:
	var msgType 	# int32
	var playerID # int32
	var id 		# string
	var salt 	# string
	var data	# JSON dict
		
	func to_json():
		var json = {}
		json["MsgData"] = {}
		json["MsgData"]["Type"] = self.msgType
		json["MsgData"]["ID"] = self.id
		json["MsgData"]["Salt"] = self.salt
		json["MsgData"]["PlayerID"] = self.playerID
		for key in data:
			if key != "MsgData":
				json[key] = data[key]
		return json

var peer
var connected = false
var id = -1 # assigned player ID
var salt = "" # generated client Salt
var server_salt = "" # the server's salt
var cached_xor_salt = ""

var connect_time = -1

var timer = null
var num_pings = 0
var ping_ids = {}

const DEFAULT_RESEND_TIME = 0.2
var resend_timer = Timer.new()
var msg_time_id = ""
var msg_sent_time = -1
var resend_time = DEFAULT_RESEND_TIME
var sent_messages = {}
var received_messages = {}

signal register_callback(status)
signal login_callback(status)

static func GenerateSalt():
	return String(OS.get_ticks_msec()).sha256_text()

static func XORSalt():
	# TODO: implement a way to xor
	pass

func packet_from_json(json):
	var packet = Packet.new()
	var msgData = json["MsgData"]
	packet.msgType = int(msgData["Type"])
	packet.id = msgData["ID"]
	packet.salt = msgData["Salt"]
	packet.playerID = msgData["PlayerID"]
	packet.data = json
	return packet

func connect_request(addr, port):
	peer = PacketPeerUDP.new()
	peer.set_send_address(addr, port)
	send_packet(new_packet(0, {"Hi": 5}))
	salt = GenerateSalt()
	print("Start timer")
	timer.start()

func _ready():
	timer = Timer.new()
	timer.set_wait_time(1.0)
	timer.connect("timeout", self, "_timer_timeout")
	self.add_child(timer)

	resend_timer.set_wait_time(resend_time)
	resend_timer.connect("timeout", self, "_resend_timer_timeout")
	self.add_child(resend_timer)
	set_process(true)

func _process(delta):
	if peer.is_listening():
		var data = peer.get_packet()
		if data.size() > 0:
			var dataStr = data.get_string_from_ascii()
			var dataJson = {}
			dataJson.parse_json(dataStr)
			handle_packet(dataJson)

func _timer_timeout():
	if num_pings >= 3:
		# we have timed out.
		num_pings = 0
		self.handle_quit()
		GameManager.peer_timed_out()
		return
	var packet = new_packet(Message.Ping, {})
	ping_ids[packet.id] = true
	num_pings += 1
	send_packet(packet)
	timer.start()

func _resend_timer_timeout():
	for packet in sent_messages.values():
		send_packet(packet, false)
	resend_timer.set_wait_time(resend_time)
	resend_timer.start()

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		self.handle_quit()

func new_packet(msgType, data):
	var packet = Packet.new()
	packet.msgType = msgType
	packet.playerID = id	
	packet.id = String(OS.get_ticks_msec())
	packet.salt = salt
	packet.data = data
	return packet

func send_packet(packet, add=true):
	var json = packet.to_json()
	var jsonStr = json.to_json().to_ascii()
	var typeStr = String(packet.msgType)
	jsonStr = String(typeStr.length()).to_ascii() + typeStr.to_ascii() + jsonStr
	if add:
		sent_messages[packet.id] = packet
	if msg_sent_time == -1:
		msg_time_id = packet.id
		msg_sent_time = OS.get_ticks_msec()
	print("-> ", format_msg_type(packet.msgType))
	peer.put_packet(jsonStr)
	if !timer.is_active():
		timer.start()
			
func handle_packet(data):
	var packet = packet_from_json(data)
	if received_messages.has(packet.id):
		pass
	print("<- ", format_msg_type(packet.msgType))
	received_messages[packet.id] = packet
	if packet.msgType == Message.Acknowledge:
		handle_ack(packet)
		return
	send_ack(packet)
	if packet.msgType == Message.ConnectionChallenge:
		handle_challenge(packet)
	elif packet.msgType == Message.ConnectionAccepted:
		handle_connection_accepted(packet)
	elif packet.msgType == Message.ConnectionDeclined:
		emit_signal("login_callback", packet.data["Error"])
	elif packet.msgType == Message.Correction:
		handle_correction(packet)
	elif packet.msgType == Message.PlayerJoined:
		handle_player_joined(packet)
	elif packet.msgType == Message.PlayerLeft:
		handle_player_left(packet)
	elif packet.msgType == Message.PlayerMoved:
		handle_player_moved(packet)
	elif packet.msgType == Message.RegisterReply:
		handle_register_reply(packet)
	elif packet.msgType == Message.ChatMessage:
		handle_chat_message(packet)

func handle_quit():
	send_packet(new_packet(Message.PlayerLeft, {"PlayerID": id}))

func send_ack(packet):
	send_packet(new_packet(Message.Acknowledge, {"ReplyTo": packet.id}))

func reset_pings():
	num_pings = 0
	ping_ids = {}

func register_request(username, password, graphic):
	# at this point password is still plaintext
	password = password.sha256_text()
	send_packet(new_packet(Message.Register, {"Username": username, "Password": password, "Graphic": graphic}))

func handle_challenge(packet):
	server_salt = packet.data["ServerSalt"]
	cached_xor_salt = packet.salt
	var reply = new_packet(Message.ConnectionChallengeReply, {"Graphic": GameManager.graphic, "Username": GameManager.username})
	send_packet(reply)

func handle_correction(packet):
	var cmd = packet.data["Command"]
	GameManager.send_command(Command.create_from_json(cmd), false)

func handle_connection_accepted(packet):
	reset_pings()

	connected = true
	self.id = packet.data["Data"]["ID"]
	var graphic = packet.data["Data"]["Graphic"]
	var pos = Vector2(packet.data["Data"]["Position"][0], packet.data["Data"]["Position"][1])
	GameManager.initialize_player(id, graphic, pos)

func handle_player_joined(packet):
	var player_id = packet.data["Data"]["ID"]
	if player_id == id:
		return
	var graphic = packet.data["Data"]["Graphic"]
	var pos = packet.data["Data"]["Position"]
	GameManager.new_entity_with_id(player_id, graphic)
	GameManager.get_entity_ref_by_id(player_id).set_name(packet.data["Username"])
	GameManager.send_command(Command.SetPos.new(player_id, Vector2(pos[0], pos[1])))

func handle_player_left(packet):
	var player_id = packet.data["PlayerID"]
	if player_id == id:
		return
	GameManager.remove_entity_by_id(player_id)

func handle_player_moved(packet):
	var player_id = packet.playerID
	if player_id == id:
		return
	var offset = packet.data["Data"]["Offset"]
	GameManager.send_command(Command.Move.new(player_id, Vector2(offset[0], offset[1])))

func handle_register_reply(packet):
	emit_signal("register_callback", packet.data["Status"])

func handle_chat_message(packet):
	GameManager.add_chat_message(packet.data["Username"], packet.data["Message"])

func handle_ack(packet):
	if ping_ids.has(packet.data["ReplyTo"]):
		num_pings -= 1
		ping_ids.erase(packet.data["ReplyTo"])
	if sent_messages.has(packet.data["ReplyTo"]):
		sent_messages.erase(packet.data["ReplyTo"])
	if msg_sent_time != -1 && packet.data["ReplyTo"] == msg_time_id:
		resend_time = (OS.get_ticks_msec() - msg_sent_time) / 1000.0