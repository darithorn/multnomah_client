extends Node2D

var Tile = preload("res://src/tile.gd")

class TileData:
	var solid = false
	var graphic = ""
	var region
	
	func _init(graphic, solid, region):
		self.graphic = graphic
		self.solid = solid
		self.region = region

var width = 0
var height = 0
var scale = Vector2(2, 2)
var spritesheet = null
var tiles = []

var tilesets = {}
var layers = {}

func load_from_tiled(filename):
	var file = File.new()
	file.open(filename, File.READ)
	if !file.is_open():
		return

	var data = {}
	data.parse_json(file.get_as_text())
	var tilesets_data = data["TileSets"]
	width = data["MapWidth"]
	height = data["MapHeight"]

	for tileset in tilesets_data:
		tilesets[tileset["ID"]] = load_tileset(tileset["File"])

	# Add Cell Size
	for layer in data["Layers"]:
		var order = layer["Name"]
		layers[order] = {}
		var tiles = []
		tiles.resize(width)
		for x in range(width):
			var ar = []
			ar.resize(height)
			tiles[x] = ar
		layers[order]["Name"] = layer["Name"]
		layers[order]["Tiles"] = tiles
		layers[order]["Data"] = layer["Data"]
		layers[order]["TileSet"] = round(layer["TileSet"])
		layers[order]["Z"] = layer["Z"]

	init_tilemap()

func load_tileset(filename):
	var file = File.new()
	file.open(filename, File.READ)
	if !file.is_open():
		return

	var data = {}
	data.parse_json(file.get_as_text())

	var tileset = {}

	tileset["Textures"] = {}	
	for texture in data["Textures"]:
		tileset["Textures"][texture["ID"]] = load(texture["File"])
	
	# Add Offset
	tileset["Tiles"] = {}		
	for id in data["Tiles"].keys():
		var tile = data["Tiles"][id]
		tileset["Tiles"][id] = {}
		tileset["Tiles"][id]["Solid"] = tile["Solid"]
		tileset["Tiles"][id]["Offset"] = tile["Offset"]
		tileset["Tiles"][id]["Z"] = tile["Z"]
		var region = tile["Region"]
		tileset["Tiles"][id]["Region"] = Rect2(region[0], region[1], region[2], region[3])
		tileset["Tiles"][id]["Texture"] = tile["Texture"]

	tileset["Tiles"]["-1"] = {}
	tileset["Tiles"]["-1"]["Solid"] = false
	tileset["Tiles"]["-1"]["Region"] = Rect2(0, 0, 16, 16)

	return tileset
	
func init_tilemap():
	self.set_scale(Vector2(2, 2))
	for layer_id in layers.keys():
		var layer = layers[layer_id]
		var tileset = tilesets[layer["TileSet"]]
		var layer_data = layer["Data"]
		var layer_obj = Node2D.new()
		self.add_child(layer_obj)
		layer_obj.set_name(layer_id)		
		layer_obj.set_z(layer["Z"])
		for x in range(width):
			for y in range(height):
				var tile_id = layer_data[x][y]
				var tile_data = tileset["Tiles"][String(tile_id)]			
				var region = tile_data["Region"]				
				layer["Tiles"][x][y] = TileData.new("", tile_data["Solid"], region)
				if tile_id == -1:
					continue
				var tile = Sprite.new()
				tile.set_centered(false)
				tile.set_script(Tile)

				tile.set_texture(tileset["Textures"][tile_data["Texture"]])
				tile.set_region(true)
				tile.set_region_rect(region)

				tile.set_z(tile_data["Z"])
				tile.set_pos(Vector2(x * 16, y * 16))
				tile.set_offset(Vector2(tile_data["Offset"][0] / 2, tile_data["Offset"][1] / 2))
				layer_obj.add_child(tile)

func set_layer_visible(name, visible):
	var node = get_node(name)
	if node != null:
		node.set_hidden(!visible)

func get_layer(name):
	return get_node(name)

func get_tiles(pos):
	var tiles = []
	for layer in layers.values():
		tiles.append(layer["Tiles"][pos.x][pos.y])
	return tiles