var texture
var scale
var sprite_size
var graphics = {}

func init(settings):
    texture = load(settings["SpriteSheet"])
    scale = Vector2(settings["DefaultScaleX"], settings["DefaultScaleY"])
    sprite_size = Vector2(settings["SpriteWidth"], settings["SpriteHeight"])
    initialize_graphics(settings)

func initialize_graphics(settings):
    if !settings.has("Sprites"):
        return
    for key in settings["Sprites"]:
        graphics[key] = settings["Sprites"][key]

func get_rect(graphic):
    return Rect2(graphics[graphic][0] * sprite_size.x,
                graphics[graphic][1] * sprite_size.y,
                sprite_size.x, sprite_size.y)