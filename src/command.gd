class Kind:
	const None = 0
	const Move = 1
	const SetPos = 2

static func create_from_json(cmd):
	var kind = cmd["Kind"]
	var id = cmd["ID"]
	if kind == Kind.Move:
		var offset = Vector2(cmd["OffsetX"], cmd["OffsetY"])
		return Move.new(id, offset)
	elif kind == Kind.SetPos:
		var pos = Vector2(cmd["X"], cmd["Y"])
		return SetPos.new(id, pos)
	return null

class Move:
	var kind = Kind.Move
	var id = -1 # entity ID to affect
	var offset = Vector2(0, 0)
	func _init(id, offset):
		self.id = id
		self.offset = offset

class SetPos:
	var kind = Kind.SetPos
	var id = -1
	var pos = Vector2(0, 0)
	func _init(id, pos):
		self.id = id
		self.pos = pos