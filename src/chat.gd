extends LineEdit

func _ready():
	set_process(true)
	set_process_input(true)

func _input(event):
	if event.is_action_pressed("enter"):
		if !has_focus():
			grab_focus()
		else:
			if !get_text().empty():
				GameManager.send_chat_message(get_text())
				set_text("")
			release_focus()