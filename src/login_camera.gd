extends Node2D

export(String) var Path = ""
export(float) var Speed = 500
export(float) var MinDistance = 5

var path_obj = null
var id = 0

func _ready():
    path_obj = get_parent().find_node(Path)
    set_process(true)

func _process(delta):
    var child = path_obj.get_child(id)
    var distance = self.get_pos().distance_to(child.get_pos())
    if distance <= MinDistance:
        id += 1
        if id >= path_obj.get_child_count():
            id = 0

    var d_pos = (child.get_pos() - get_pos()).normalized()
    d_pos *= Speed * delta
    translate(d_pos)