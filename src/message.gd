extends Label

const COLOR_GREEN = Color(0, 255, 0)
const COLOR_BLUE = Color(0, 0, 255)
const COLOR_RED = Color(255, 0, 0)

func init(username, message):
	self.set_message_text(username, message)

func set_text(text):
	.set_text(text)

func set_message_text(username, message):
	set_text("[" + username + "]: " + message)