tool
extends EditorPlugin

const EXPORT_TILEMAP = 0
const EXPORT_TILESET = 1

var dock
var popup
var root

var filename = ""
var width = 0
var height = 0
var tilesets = []
var layers = []

var textures = []
var tiles = {}

func _enter_tree():
    dock = preload("res://addons/tilemap_exporter/tilemap_exporter_dock.tscn").instance()
    var export_button = dock.find_node("export")
    popup = export_button.get_popup()
    popup.add_item("TileMap", EXPORT_TILEMAP)
    popup.add_item("TileSet", EXPORT_TILESET)
    popup.connect("item_pressed", self, "export_button_pressed")
    add_control_to_container(CONTAINER_CANVAS_EDITOR_MENU, dock)

func _exit_tree():
    remove_control_from_docks(dock)
    dock.free()

func export_button_pressed(id):
    root = get_tree().get_edited_scene_root()
    if root == null:
        return
    filename = root.get_name() + ".json"    
    width = 0
    height = 0
    tilesets = []
    layers = []
    textures = []
    tiles = {}   
    if id == EXPORT_TILEMAP:
        export_tilemap()
    elif id == EXPORT_TILESET:
       export_tileset()
    
func export_tilemap():
    width = root.Width
    height = root.Height
    for i in range(root.get_child_count()):
        var child = root.get_child(i)
        if child.is_type("TileMap"):
            var layer = export_tilemap_layer(child, i)
            layers.append(layer)
    var tilemap = {}
    tilemap["TileSets"] = tilesets
    tilemap["MapWidth"] = width
    tilemap["MapHeight"] = height
    tilemap["PlayerSpawn"] = [root.PlayerSpawn.x, root.PlayerSpawn.y]
    tilemap["Layers"] = layers
    var file = File.new()
    file.open("res://data/tilemaps/" + filename, File.WRITE)
    file.store_string(tilemap.to_json())
    file.close()

func get_tileset_id(path):
    var name = "res://data/tilesets/" + path.split(".", false)[0] + ".json"
    var last_id = 0
    for t in tilesets:
        last_id = t["ID"]
        if t["File"] == name:
            return last_id
    tilesets.append({"ID": last_id + 1, "File": name})
    return last_id + 1

func export_tilemap_layer(child, i):
    var layer = {}
    layer["Name"] = child.get_name()
    var tileset = child.get_tileset()
    var path = tileset.get_path().split("/")
    layer["TileSet"] = get_tileset_id(path[path.size() - 1])
    layer["Order"] = i
    var data = []
    for x in range(width):
        data.append([])
        for y in range(height):
            data[x].append(child.get_cell(x, y))
    layer["Data"] = data
    layer["Z"] = child.get_z()
    return layer

func export_tileset():
    for i in range(root.get_child_count()):
        var child = root.get_child(i)
        if child.is_type("Sprite"):
            var data = export_sprite(child)
            tiles[String(i)] = data
    var tileset = {}
    tileset["Textures"] = textures
    tileset["Tiles"] = tiles
    var file = File.new()
    file.open("res://data/tilesets/" + filename, File.WRITE)
    file.store_string(tileset.to_json())
    file.close()        

func get_texture_id(name):
    var last_id = 0
    for t in textures:
        last_id = t["ID"]
        if t["File"] == name:
            return last_id
    textures.append({"ID": last_id + 1, "File": name})
    return last_id + 1

func export_sprite(child):
    var data = {}
    data["Texture"] = get_texture_id(child.get_texture().get_path())
    var rect = child.get_region_rect()
    data["Region"] = [rect.pos.x, rect.pos.y, rect.size.x, rect.size.y]
    data["Solid"] = child.Solid
    data["Offset"] = [child.get_offset().x, child.get_offset().y]
    data["Z"] = child.get_z()
    return data